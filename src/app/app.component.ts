import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bienvenido a Angular!';
  curso = 'Trabajando con Spring y Angular 9';
  autor = 'Carlos Martin Milan';
}
