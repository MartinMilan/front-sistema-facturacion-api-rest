import { Injectable } from '@angular/core';
import { Cliente } from './cliente';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Region } from './region';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  /**
   * Vamos a definir una variable que va a contener el endPoint a través del cual vamos a realizar
   * la consulta o petición al servidor para obtener los clientes
   */
  private urlEndPoint = 'http://localhost:8080/api/clientes';
  // private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) { }

  // private agregarAuthorizationHeader() {
  //   // tslint:disable-next-line: prefer-const
  //   let token = this.authService.TokenAcceso;
  //   if (token != null) {
  //     return this.httpHeaders.append('Authorization', 'Bearer ' + token);
  //   }
  //   return this.httpHeaders;
  // }

  /**
   * Implemento el patrón de diseño Observador, en este caso Clientes[] será la variables que estará
   * siendo observada, de modo que cualquier cambio en su estado será notificado a los observadores y
   * este disparará algun evento
   *
   * el operador tap no realiza subscribe pero nos permite realizar algunas operaciones con el flujo
   * son modificar el mismo. El map modifica el flujo (por ejemplo abajo lo hace de response a un
   * arreglo de clientes), mientras que el tap no lo cambia en absoluto
   */
  getClientes(page: number): Observable<any> {
    // return of(CLIENTES);
    return this.http.get<Cliente[]>(this.urlEndPoint + '/page/' + page).pipe(
      tap((response: any) => {
        // const clientes = response as Cliente[]; // Si bien convierto response a Cliente[], no se refleja en el pipe
        (response.content as Cliente[]).forEach(cliente => { // response as Cliente es equivalente al cast (Cliente[]) response
          console.log(cliente.nombre);
        });
      }),
      map((response: any) => {
        // const clientes = response as Cliente[];
        (response.content as Cliente[]).map(cliente => {
          cliente.nombre = cliente.nombre.toUpperCase();
          // registerLocaleData(localeES, 'es'); Vamos a sacar el localDate para llevarlo a una localidad
          // mas global dentro del proyecto (app.module)
          const datePipe = new DatePipe('es');
          cliente.createAt = datePipe.transform(cliente.createAt, 'EEEE dd, MMMM yyyy');
          return cliente;
        });
        return response;
      })
    );
    /**
     * Otra forma de conseguir los clientes del json devuelto como respuesta es la siguiebte
     * return this.http.get(this.urlEndPoint).pipe(map( response => response as Cliente[]));
     * NOTA: Si quisiera cambiar datos del flujo de entrada, como convertir todos los nombres a
     * mayúsculas, lo puedo hacer manipulando el response de la siguiente manera:
     * return this.http.get(this.urlEndPoint).pipe(map( response => {
     *    let clientes = response as Cliente[];
     *    return clientes.map(cliente => { // Recibe como argumento cada cliente del listado
     *      cliente.nombre = cliente.nombre.toUpperCase(); // Nombre a mayúsculas
     *      cliente.createAt = formatDate(cliente.createAt, 'dd-MM-yyyy', 'en-US'); // import { formatDate } from '@angular/common'
     *
     *      let datePipe = new DatePipe('en-US'); // Otra forma de convertir la fecha: import { DatePipe } from '@angular/common'
     *      cliente.createAt = datePipe.transform(cliente.createAt, 'dd/MM/yyyy');
     *
     *      return cliente;
     *    });
     *  })
     * );
     */
  }

  /**
   * Este método enviará el cliente que se va a insertar en la BD y el servicio rest retornará
   * el cliente insertado que acá es recibido como un observable
   * @param cliente representa el cliente que se va a insertar en la BD (será enviado en la petición)
   */
  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post(this.urlEndPoint, cliente).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  /**
   * Con el siguiente método vamos a obtener un cliente por Id
   */
  getCliente(id): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`).pipe( // Con pipe obtenemos los operadores del flujo
      catchError(e => { // Se captura cualquier método de error (40x 50x) y lo encapsula en 'e'
        if (e.status !== 401 && e.error.mensaje) {
          this.router.navigate(['/clientes']);
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  /**
   * Con el siguiente método vamos a actualizar los datos de un cliente
   */
  update(cliente: Cliente): Observable<Cliente> {
    return this.http.put(`${this.urlEndPoint}/${cliente.id}`, cliente).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  /**
   * Con el siguiente método vamos a eliminar a un cliente
   */
  delete(id: number): Observable<Cliente> {
    return this.http.delete(`${this.urlEndPoint}/${id}`).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e => {
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>> {
    // tslint:disable-next-line: prefer-const
    let formData = new FormData();
    formData.append('archivo', archivo);
    formData.append('id', id);

    // tslint:disable-next-line: prefer-const
    let req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, { reportProgress: true });
    return this.http.request(req);
  }

  getRegiones(): Observable<Region[]> {
    return this.http.get<Region[]>(this.urlEndPoint + '/regiones');
  }
}
