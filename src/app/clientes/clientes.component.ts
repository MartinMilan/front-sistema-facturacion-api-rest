import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from './detalle/modal.service';
import { AuthService } from '../usuarios/auth.service';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  paginador: any;
  clienteSeleccionado: Cliente;
  /**
   * De la siguiente manera estoy creando una propiedad e inyectandola al mismo tiempo, sería
   * equivalente a: private clienteService: ClienteService; y luego pasar una instancia de
   * ClienteService por el constructor
   * @param clienteService inyectando una instancia de clienteService
   */
  constructor(private clienteService: ClienteService, private activateRoute: ActivatedRoute,
// tslint:disable-next-line: align
              public modalService: ModalService, public authService: AuthService) { }

  /**
   * Ahora en getClientes hay un observador, por lo tanto debemos subscribir nuestra variable clientes
   * a dicho observador.
   * clientes => this.clientes = clientes: es una función anónima donde el clientes a la izquierda
   * de la flecha representa el parámetro de la función anónima y lo que esta a la derecha de la
   * flecha representa el cuerpo de la función (sería como un set).
   * Si existira más de un parámetro en la función anónima y más de una sentencia en el cuerpo, la
   * sintaxis seria asi: (param1, param2) => {this.param1 = param1; this.param2 = param2}
   */
  ngOnInit() {
    // this.clientes = this.clienteService.getClientes();
    this.activateRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if (!page) {
        page = 0;
      }
      this.clienteService.getClientes(page).subscribe(response => {
        this.clientes = response.content as Cliente[];
        this.paginador = response;
      });
    });
    this.modalService.notificarUpload.subscribe(cliente => {
      this.clientes = this.clientes.map(clienteOriginal => {
        if (cliente.id === clienteOriginal.id) {
          clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
      });
    });
  }

  delete(cliente: Cliente): void {

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success mx-1',
        cancelButton: 'btn btn-danger mx-1'
      },
      buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
      title: '¿Estás seguro?',
      text: `¿Seguro que deseas borrar a ${cliente.nombre} ${cliente.apellido}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.clienteService.delete(cliente.id).subscribe(response => {
          this.clientes = this.clientes.filter(cli => cli !== cliente);
          swalWithBootstrapButtons.fire(
            '¡Borrado!',
            `El cliente ${cliente.nombre} ${cliente.apellido} ha sido eliminado`,
            'success'
          );

        });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El cliente no se ha borrado',
          'error'
        );
      }
    });
  }

  abrirModal(cliente: Cliente): void {
    this.clienteSeleccionado = cliente;
    this.modalService.abrirModal();
  }

}
