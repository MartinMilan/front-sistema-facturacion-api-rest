import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  modal = false;
  private NotificarUpload = new EventEmitter<any>();
  constructor() { }

  get notificarUpload(): EventEmitter<any> {
    return this.NotificarUpload;
  }

  abrirModal(): void {
    this.modal = true;
  }

  cerrarModal(): void {
    this.modal = false;
  }

  isModal(): boolean {
    return this.modal;
  }
}
