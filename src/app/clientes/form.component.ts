import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Region } from './region';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  cliente: Cliente = new Cliente();
  regiones: Region[];
  titulo = 'Crear cliente';
  errores: string[];

  constructor(private clienteService: ClienteService, private router: Router, private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarCliente();
    this.clienteService.getRegiones().subscribe(regiones => this.regiones = regiones);
  }

  public create(): void {
    this.clienteService.create(this.cliente).subscribe( cliente => {
      this.router.navigate(['/clientes']); // el router es necesario para redirigir a otra página
      Swal.fire('Cliente Registrado', `Cliente ${cliente.nombre} ${cliente.apellido} creado con exito`, 'success');
    }, err => {
      this.errores = err.error.errors as string[];
    }
    );
  }

  cargarCliente(): void {
    this.activateRoute.params.subscribe(params => {
      const ident = 'id';
      const id = +params[ident];
      if (id) {
        this.clienteService.getCliente(id).subscribe(cliente => this.cliente = cliente);
      }
    });
  }

  update(): void {
    this.cliente.facturas = null;
    this.clienteService.update(this.cliente).subscribe( cliente => {
      this.router.navigate(['/clientes']);
      Swal.fire('Cliente Actualizado', `Cliente ${this.cliente.nombre} ${this.cliente.apellido} actualizado con exito`, 'success');
    }, err => {
      this.errores = err.error.errors as string[];
    });
  }

  compararRegion(o1: Region, o2: Region): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }
    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }

}
