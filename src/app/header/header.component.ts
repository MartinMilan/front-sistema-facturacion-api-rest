import { Component } from '@angular/core';
import { AuthService } from '../usuarios/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  titulo = 'App Angular';
  constructor(public authService: AuthService, private router: Router) {}

  logout(): void {
    const uname = this.authService.UsuarioToken.username;
    this.authService.logout();
    Swal.fire('Se ha cerrado sesión', `Hasta luego ${uname}!, has cerrado sesión con éxito`, 'success');
    this.router.navigate(['/clientes']);
  }
}
