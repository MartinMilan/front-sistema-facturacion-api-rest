import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private usuarioToken: Usuario;
  private tokenAcceso: string;

  constructor(private http: HttpClient) { }

  public get UsuarioToken(): Usuario {
    if (this.usuarioToken != null) {
      return this.usuarioToken;
    } else if (this.usuarioToken == null && sessionStorage.getItem('usuario') != null) {
      this.usuarioToken = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
      return this.usuarioToken;
    }
    return new Usuario();
  }

  public get TokenAcceso(): string {
    if (this.tokenAcceso != null) {
      return this.tokenAcceso;
    } else if (this.tokenAcceso == null && sessionStorage.getItem('token') != null) {
      this.tokenAcceso = sessionStorage.getItem('usuario');
      return this.tokenAcceso;
    }
    return null;
  }

  login(usuario: Usuario): Observable<any> {
    const urlEndpoint = 'http://localhost:8080/oauth/token';

    const credenciales = btoa('angularapp' + ':' + '12345');

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic ' + credenciales
    });

    const params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', usuario.username);
    params.set('password', usuario.password);

    return this.http.post<any>(urlEndpoint, params.toString(), { headers: httpHeaders });
  }

  guardarUsuario(accessToken: string): void {
    const payload = this.obtenerDatosToken(accessToken);
    this.usuarioToken = new Usuario();
    this.usuarioToken.nombre = payload.nombre;
    this.usuarioToken.apellido = payload.apellido;
    this.usuarioToken.email = payload.email;
    this.usuarioToken.username = payload.user_name;
    this.usuarioToken.roles = payload.authorities;
    sessionStorage.setItem('usuario', JSON.stringify(this.usuarioToken));
  }

  guardarToken(accessToken: string): void {
    this.tokenAcceso = accessToken;
    sessionStorage.setItem('token', this.tokenAcceso);
  }

  obtenerDatosToken(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split('.')[1]));
    }
    return null;
  }

  isAuthenticated(): boolean {
    // tslint:disable-next-line: prefer-const
    let payload = this.obtenerDatosToken(this.TokenAcceso);
    if (payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }

  logout(): void {
    this.usuarioToken = null;
    this.tokenAcceso = null;
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('token');
  }

  hasRole(role: string): boolean {
    if (this.UsuarioToken.roles.includes(role)) {
      return true;
    }
    return false;
  }
}
