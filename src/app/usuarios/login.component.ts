import { Component, OnInit } from '@angular/core';
import { Usuario } from './usuario';
import Swal from 'sweetalert2';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  titulo = 'Por favor inicie sesión!';
  usuario: Usuario;
  constructor(private authService: AuthService, private router: Router) {
    this.usuario = new Usuario();
  }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      Swal.fire('Sesión Iniciada', `Hola ${this.authService.UsuarioToken.username} ya estás autenticado`, 'info');
      this.router.navigate(['/clientes']);
    }
  }

  login(): void {
    console.log(this.usuario);
    if (this.usuario.username == null || this.usuario.password == null) {
      Swal.fire('Error de inicio de sesión', 'Nombre de usuario o Contraseña vacíos!', 'error');
      return;
    }
    this.authService.login(this.usuario).subscribe(response => {
      console.log(response);

      this.authService.guardarUsuario(response.access_token);
      this.authService.guardarToken(response.access_token);

      const usuario: Usuario = this.authService.UsuarioToken;

      this.router.navigate(['/clientes']);
      Swal.fire('Se ha iniciado sesión', `Bienvenido ${usuario.username}!, has iniciado sesión con éxito`, 'success');
    }, error => {
      if (error.status === 400) {
        Swal.fire('Error al iniciar sesión', 'Usuario o Contraseña incorrectas!', 'error');
      }
    });
  }

}
